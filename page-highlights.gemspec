$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'page-highlights/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'page-highlights'
  s.version     = PageHighlights::VERSION
  s.authors     = ['Nic Haynes', 'Madeline Cowie']
  s.email       = ['nic@nicinabox.com', 'madeline@cowie.me']
  s.summary     = 'A simple promotions engine'
  s.description = 'A simple promotions engine'

  s.files = Dir['{app,config,db,lib,vendor}/**/*'] + ['Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'montage', '> 1.3.1'
  s.add_dependency 'pubdraft', '>= 0.1.0'
  s.add_dependency 'rails', '>= 4.0'
end
