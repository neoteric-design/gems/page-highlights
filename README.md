# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# PageHighlights

## Install

    rails g page_highlights:install

## Active Admin

Edit `app/views/admin/page_highlights/_form.html.erb` as necessary.

    rails g page_highlights:activeadmin

## Using Uploader

In Gemfile

    gem "montage"

In `app/models/page_highlight.rb`

    montage
