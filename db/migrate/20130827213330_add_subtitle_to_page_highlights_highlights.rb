class AddSubtitleToPageHighlightsHighlights < ActiveRecord::Migration
  def change
    add_column :page_highlights_highlights, :subtitle, :string
  end
end
