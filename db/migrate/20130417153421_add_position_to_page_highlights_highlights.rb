class AddPositionToPageHighlightsHighlights < ActiveRecord::Migration
  def change
    add_column :page_highlights_highlights, :position, :integer
  end
end
