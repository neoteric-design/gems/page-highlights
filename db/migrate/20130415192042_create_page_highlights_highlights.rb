class CreatePageHighlightsHighlights < ActiveRecord::Migration
  def change
    create_table :page_highlights_highlights do |t|
      t.string :title
      t.text :body
      t.string :url
      t.text :image_urls
      t.string :state
      t.boolean :featured

      t.timestamps
    end
  end
end
