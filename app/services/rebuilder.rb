module Rebuilder
  def self.included(base)
    base.send(:collection_action, :rebuild, :method => :post) do
      params[param_name].each_with_index do |id, index|
        resource_class.where(id: id).update_all(position: index + 1)
      end

      render nothing: true
    end

    base.controller do
      private
      def param_name
        active_admin_config.resource_label.downcase.gsub(' ', '_').to_sym
      end
    end
  end
end
