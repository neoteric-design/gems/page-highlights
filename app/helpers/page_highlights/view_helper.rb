module PageHighlights
  module ViewHelper
    def published_page_highlights
      @published_page_highlights ||= PageHighlight.published
    end

    def home_page_highlights
      @home_page_highlights ||= PageHighlight.published.featured
    end
  end
end
