module PageHighlights
  class Highlight < ActiveRecord::Base
    default_scope -> { order('position ASC') }
    scope :featured, -> { where(featured: true) }
  end
end
