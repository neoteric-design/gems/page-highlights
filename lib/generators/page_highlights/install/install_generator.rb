module PageHighlights
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def add_model
      copy_file "models_page_highlight.rb", "app/models/page_highlight.rb"
    end

    def install_migrations
      rake "railties:install:migrations"
    end
  end
end