ActiveAdmin.register PageHighlight do
  include Rebuilder
  config.sort_order = 'position_asc'

  filter :title
  filter :subtitle
  filter :body
  filter :url
  filter :state, as: :select
  filter :featured
  filter :created_at
  filter :updated_at

  form :partial => 'form'

  permit_params do
    PageHighlights.params
  end

  index do
    column :title
    column :state do |item|
      span item.state, :class => "state-label #{item.state}"
    end

    column :sort do
      a :class => 'handle',
        :href => rebuild_admin_page_highlights_path do
        i :class => 'fa fa-bars'
      end
    end

    actions
  end
end
