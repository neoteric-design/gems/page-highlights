module PageHighlights
  class ActiveadminGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def copy_templates
      copy_file "admin_page_highlights.rb", "app/admin/page_highlights.rb"
      copy_file "admin__form.html.erb", "app/views/admin/page_highlights/_form.html.erb"
    end
  end
end