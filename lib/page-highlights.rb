require "page-highlights/engine"

module PageHighlights
  def self.params
    [:body, :featured, :image_urls, :state, :title, :subtitle, :url, gallery_ids: []]
  end
end
