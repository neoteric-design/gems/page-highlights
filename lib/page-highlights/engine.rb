module PageHighlights
  class Engine < ::Rails::Engine
    isolate_namespace PageHighlights

    config.to_prepare do
      ApplicationController.helper(PageHighlights::ViewHelper)
    end
  end
end
